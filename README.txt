
INTRODUCTION
------------

This module provides a basic functionality for 
heureka.sk or heureka.cz - feedback and conversion.


INSTALLATION
------------

 1. Drop the 'commerce_heureka' folder into the modules directory
    '/sites/all/modules/'.

 2. In your Drupal site, enable the module under Administration -> Modules
    '?q=/admin/modules'.

 3. Setup new heureka display under 
Store -> Configuration -> Heureka settings '?q=admin/commerce/config/heureka'.
