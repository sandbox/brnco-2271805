<?php

/**
 * @file
 * Commerce Heureka administration menu items.
 */

/**
 * Form builder for Heureka settings form.
 *
 * @ingroup forms
 */
function commerce_heureka_settings_form() {
  $form['wrapper'] = array('#type' => 'fieldset', '#title' => t('Heureka API settings'));
  $form['wrapper']['commerce_heureka_api_key_field'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('commerce_heureka_api_key_field', ''),
  );

  $form['wrapper']['commerce_heureka_api_conversion_key_field'] = array(
    '#type' => 'textfield',
    '#title' => t('API conversion key'),
    '#default_value' => variable_get('commerce_heureka_api_conversion_key_field', ''),
  );

  $form['wrapper']['commerce_heureka_api_country_code'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => array(
      '' => t('Choose country'),
      'sk' => t('SK'),
      'cz' => t('CZ'),
    ),
    '#default_value' => variable_get('commerce_heureka_api_country_code', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Form validation handler for commerce_heureka_settings_form().
 *
 * @see system_settings_form_submit()
 */
function commerce_heureka_settings_form_validate($form, &$form_state) {
  // Check API key.
  if (!preg_match('~^[a-zA-Z0-9]+$~', $form_state['values']['commerce_heureka_api_key_field'])) {
    form_set_error('commerce_heureka_api_key_field', t('Please fill correct API key!'));
  }
  // Check API JS conversion key.
  if (!preg_match('~^[a-zA-Z0-9]+$~', $form_state['values']['commerce_heureka_api_conversion_key_field'])) {
    form_set_error('commerce_heureka_api_conversion_key_field', t('Please fill correct API conversion key!'));
  }
}
